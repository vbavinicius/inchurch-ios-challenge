//
//  Helper.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 09/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

class Helper {
    // Function to set back button title as empty
    static func configureNavBarBackButton(vc: UIViewController) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        vc.navigationItem.backBarButtonItem = backItem
    }
    // Function the goes to detail movie VC with selected movie
    static func goToDetailView(movie: MovieRealm, currentVC: UIViewController) {
        // Get the right view controller
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "detailVC") as! MovieDetailViewController
        // Set movie
        detailVC.movie = movie
        // Perform vc change
        DispatchQueue.main.async {
            currentVC.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}
