//
//  FavoriteMovieUITableViewCell.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 09/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

class FavoriteMovieUITableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var movieDescription: UITextView!
    @IBOutlet weak var dateReleaseLabel: UILabel!
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        customizeCell()
    }
    
    //MARK: - Customize
    func customizeCell() {
        // BG Color
        self.containerView.backgroundColor = grayColor
        // Text View
        self.movieDescription.textContainer.maximumNumberOfLines = 0
        self.movieDescription.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.movieDescription.textContainerInset = .zero
        self.movieDescription.textContainer.lineFragmentPadding = 0
        // Text Colors
        self.movieTitleLabel.textColor = mainColor
        self.movieDescription.textColor = .white
        self.dateReleaseLabel.textColor = .white
        // Shadows
        movieImageView.applyShadows()
    }
    
    //MARK: - Configure
    // Function to configure cell in CellForRow method
    func configureCell(movie: MovieRealm) {
        movieTitleLabel.text = movie.title
        movieDescription.text = movie.overview
        dateReleaseLabel.text = movie.release_date
        KFService.shared.setPosterImage(imageView: &movieImageView, movie: movie)
    }

}
