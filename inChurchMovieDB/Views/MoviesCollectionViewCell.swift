//
//  MoveisCollectionViewCell.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

// Protocol
protocol FavoriteSelectionDelegate: class {
    // Function to detect when favorite button is tapped on collection view cell
    func favoriteButtonDidPressed(movie: MovieRealm, cell: UICollectionViewCell)
}


class MoviesCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieLabel: UILabel!
    @IBOutlet weak var favoriteStarImageView: UIImageView!
    @IBOutlet weak var favoriteTouchView: UIView!
    
    //MARK: - Delegate
    weak var delegate: FavoriteSelectionDelegate?
    
    //MARK: - Variables
    var movie: MovieRealm!
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customizeCell()
        setupGesture()
    }
    
    //MARK: - Customize
    func customizeCell() {
        // Shadows
        movieImageView.applyShadows()
    }
    
    //MARK: - Tap Gesture
    // Setup gesture recognizer to favorite movies
    func setupGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleFavoriteTap))
        tapGesture.numberOfTapsRequired = 1
        self.favoriteTouchView.isUserInteractionEnabled = true
        self.favoriteTouchView.addGestureRecognizer(tapGesture)
    }
    // Handle gesture
    @objc func handleFavoriteTap() {
        delegate?.favoriteButtonDidPressed(movie: movie, cell: self)
    }
    
    //MARK: - Configure
    // Function to configure cell in CellForItem method
    func configureCell(movie: MovieRealm) {
        // Set movie variable as received movie
        self.movie = movie
        // UI
        movieLabel.text = movie.title
        KFService.shared.setPosterImage(imageView: &movieImageView, movie: movie)
        // Check if movie is favorite in order to set correct star icon
        if movie.isFavorite {
            favoriteStarImageView.image = UIImage(named: "star-green-filled")
        } else {
            favoriteStarImageView.image = UIImage(named: "star-green-empty")
        }
    }
}
