//
//  WarningView.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 09/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

class WarningView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    // Awake from nib
    override func awakeFromNib() {
        print("loaded")
    }
    
    
    
    static func initFromNib() -> WarningView {
        return Bundle.main.loadNibNamed("WarningView", owner: self, options: nil)?.first as! WarningView
    }

}
