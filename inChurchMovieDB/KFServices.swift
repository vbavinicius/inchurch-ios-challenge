//
//  File.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 09/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import Kingfisher

// Class to serve KingFisher pods based functions as a Singleton
class KFService {
    
    // Create a singleton
    private init() {}
    static let shared = KFService()

    func setPosterImage(imageView: inout UIImageView, movie: MovieRealm) {
        if let posterURL = URL(string: movie.posterURLString) {
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(
                with: posterURL,
                placeholder: UIImage(named: "movie-placeholder"),
                options: [
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(0.3)),
                    .cacheOriginalImage
                ]
            )
        }
    }

}
