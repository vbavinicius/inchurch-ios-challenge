//
//  Networking.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit
import Alamofire

// Class to serve Networking based functions as a Singleton using Alamofire as HTTP Framework
class NetworkingService {
    
    // Create a singleton
    private init() {}
    static let shared = NetworkingService()
    
    // API Key
    let API_KEY = "4effe2594c65f0b7de08d4ca3da9307e"
    
    // Function to fetch trending movies from API
    func getTrendingMovies(page: Int, completionHandler: @escaping () -> ()){
        // Base URL
        guard let trendingURLBase = URL(string: "https://api.themoviedb.org/3/movie/popular") else {return}
        // URL Parameters
        let parameters = [
            "api_key": API_KEY,
            "language": "en-US",
            "page": String(page)
        ]
        // Make a request
        AF.request(trendingURLBase, parameters: parameters, encoding: URLEncoding.default).responseData { (response) in
            // Check errors
            if let error = response.error {
                print("Failed to contact API", error)
                return
            }
            // Check if there is data
            guard let data = response.data else { return }
            do {
                // Decode data and parse as JSON
                let searchResult = try JSONDecoder().decode(APIResults.self, from: data)
                // Extract movies objects from JSON
                guard let movies = searchResult.results else {return}
                // Loop throw all movies and save as MovieRealm objects into disk
                for movie in movies {
                    // If the object is already in Realm, just update poster and popularity
                    if RealmService.shared.getRealmMovie(movie: movie) != nil {
                        RealmService.shared.updatePopularityAndPoster(movie: movie)
                    // If not, create Movie Realm object
                    } else {
                        RealmService.shared.create(MovieRealm(movie: movie))
                    }
                }
                // Return MovieRealm objects as a completion Handler out of main thread
                DispatchQueue.main.async {
                    completionHandler()
                }
            // Check decode errors
            } catch let decodeErr {
                print("Failed to decode:", decodeErr)
            }
        }
    }
    
    
    // Function to fetch movie genres from API and save direct to Realm
    func getGenresAndSaveToRealm() {
        // Base URL
        guard let genresURLBase = URL(string: "https://api.themoviedb.org/3/genre/movie/list") else {return}
        // URL Parameters
        let parameters = [
            "api_key": API_KEY,
            "language": "en-US"
        ]
        // Make a request
        AF.request(genresURLBase, parameters: parameters, encoding: URLEncoding.default).responseData { (response) in
            // Check errors
            if let error = response.error {
                print("Failed to contact API", error)
                return
            }
            // Check if there is data
            guard let data = response.data else { return }
            do {
                // Decode data and parse as JSON
                let genreResults = try JSONDecoder().decode(GenreResults.self, from: data)
                // Extract genres objects from JSON
                guard let genres = genreResults.genres else {return}
                // Save genre objects into Realm
                DispatchQueue.main.async {
                    for genre in genres {
                        RealmService.shared.createGenre(genre)
                    }
                }
                
                // Check decode errors
            } catch let decodeErr {
                print("Failed to decode:", decodeErr)
            }
        }
    }

}
