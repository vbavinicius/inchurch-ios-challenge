//
//  Movie.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import Foundation
import RealmSwift

// Movie Realm Object to save into Realm DB and persist in disk
class MovieRealm: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var release_date: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var genres: String = ""
    @objc dynamic var posterURLString: String = ""
    @objc dynamic var isFavorite: Bool = false
    @objc dynamic var popularity: Float = 0.0
    var genresList = List<Genre>()
    
    // Make ID as object primary key
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // Convenience init from movie object that is returned from API
    convenience init(movie: Movie) {
        self.init()
        self.id = movie.id
        self.title = movie.title
        self.release_date = movie.release_date
        self.overview = movie.overview
        self.genres = movie.movieGenres()
        self.popularity = movie.popularity
        if let posterURLString = movie.posterPathURL()?.absoluteString {
            self.posterURLString = posterURLString
        }
        for genre_id in movie.genre_ids {
            if let genreRealm = RealmService.shared.realm.object(ofType: Genre.self, forPrimaryKey: genre_id) {
                genresList.append(genreRealm)
            }
        }
    }
}


// Movie class with decodable to receive and decode info from API
class Movie: Decodable {
    var id: Int!
    var title: String!
    var release_date: String!
    var overview: String!
    var genre_ids: [Int]!
    var poster_path: String!
    var popularity: Float!
    
    // Generate poster URL from API
    func posterPathURL() -> URL? {
        guard let posterPath = self.poster_path else {return nil}
        if let posterURL = URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(posterPath)") {
            return posterURL
        }
        return nil
    }
    
    // Function to format genresIDs into a formated string based on Genre Realm Objects
    func movieGenres() -> String {
        // Initial empty string
        var formattedGenre = ""
        // Loop throw all related genres
        for (index, genreID) in genre_ids.enumerated() {
            let count = genre_ids.count - 1
            // Check order itens to discover the order and format the string in correct way.
            if let genreRealm = RealmService.shared.realm.object(ofType: Genre.self, forPrimaryKey: genreID) {
                if index < count - 1 {
                    formattedGenre += "\(genreRealm.name), "
                } else if index == count - 1 {
                    formattedGenre += "\(genreRealm.name) and "
                } else if index == count {
                    formattedGenre += "\(genreRealm.name)"
                }
            }
        }
        // Return full string
        return formattedGenre
    }
}

// Struct to help to decode movie results from API
struct APIResults: Decodable {
    var results: [Movie]?
}
