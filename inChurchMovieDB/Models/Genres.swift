//
//  Genres.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 08/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import Foundation
import RealmSwift

// Movie Realm Object to save into Realm DB and persist in disk
class Genre: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    
    // Make ID as object primary key
    override static func primaryKey() -> String? {
        return "id"
    }
}

// Struct to help to decode genre results from API
struct GenreResults: Decodable {
    var genres: [Genre]?
}
