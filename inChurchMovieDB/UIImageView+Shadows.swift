//
//  UIImageView+Shadows.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 08/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

extension UIImageView {
    // Function to apply shadows in ImageViews
    func applyShadows() {
        super.clipsToBounds = false
        super.layer.masksToBounds = false
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 2.0
    }
}
