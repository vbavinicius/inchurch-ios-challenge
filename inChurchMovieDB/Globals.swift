//
//  Globals.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit

let mainColor = UIColor(red: 167/255, green: 208/255, blue: 87/255, alpha: 1) //Inchurch color
let grayColor = UIColor(red: 35/255, green: 35/255, blue: 35/255, alpha: 1)
