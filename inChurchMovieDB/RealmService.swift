//
//  File.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 08/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import RealmSwift

// Class to serve Realm based functions as a Singleton
class RealmService {
    
    // Create a singleton
    private init() {}
    static let shared = RealmService()
    
    // Init Realm
    var realm = try! Realm()
    
    // Create Movie Realm Objects
    func create(_ object: MovieRealm) {
        do {
            try realm.write {
                realm.add(object, update: true)
            }
        } catch {
            postRealmError(error)
        }
    }
    
    // Read Movie Realm Objects
    func read(object: MovieRealm.Type) -> Results<MovieRealm> {
        return realm.objects(object)
    }
    
    // Get Movie Realm Object from Movie Object
    func getRealmMovie(movie: Movie) -> MovieRealm? {
        return realm.object(ofType: MovieRealm.self, forPrimaryKey: movie.id)
    }
    
    // Update pouplarity and poster on new fetchs from server
    func updatePopularityAndPoster(movie: Movie) {
        // Get the Movie Realm Object
        if let moveToUpdate = RealmService.shared.getRealmMovie(movie: movie) {
            do {
                // Write new parameters into Realm
                try realm.write {
                    moveToUpdate.popularity = movie.popularity
                    if let posterURLString = movie.posterPathURL()?.absoluteString {
                        moveToUpdate.posterURLString = posterURLString
                    }
                    realm.add(moveToUpdate, update: true)
                }
            } catch {
                postRealmError(error)
            }
        }
    }
    
    // Update Favorite Status
    func updateFavoriteStatus(movie: MovieRealm, to status: Bool) {
        do {
            try realm.write {
                movie.isFavorite = status
                realm.add(movie, update: true)
            }
        } catch {
            postRealmError(error)
        }
    }
    
    // Create Genre
    func createGenre(_ object: Genre) {
        do {
            try realm.write {
                realm.add(object, update: true)
            }
        } catch {
            postRealmError(error)
        }
    }

    // Show realm path
    func printRealPath() {
        guard let realmPath = realm.configuration.fileURL?.absoluteString else {return}
        print(realmPath)
    }
    
    // Handle Errors Notifications
    func postRealmError(_ error: Error) {
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observeRealmsErrors(in vc: UIViewController, completion: @escaping (Error?) -> Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"), object: nil, queue: nil) { (notification) in
            completion(notification.object as? Error)
        }
    }
    
    func stopObserveRealmErrors(in vc: UIViewController) {
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
}
