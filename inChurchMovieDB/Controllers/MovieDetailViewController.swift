//
//  MovieDetailViewController.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit
import RealmSwift

class MovieDetailViewController: UIViewController {

    // Variable that holds movie to display in DetailVC
    var movie: MovieRealm!
    
    //MARK: - IBOutlets
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieDescriptionLabel: UITextView!
    @IBOutlet weak var movieDateReleaseLabel: UILabel!
    @IBOutlet weak var movieGenresListLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize VC
        customizeVC()
        // Set movie info
        updateUIInfo()
        // Check favorite status to show write button info and function
        configureButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Check favorite status to show write button info and function
        configureButton()
    }

    // MARK: - Setup methods
    private func customizeVC() {
        self.title = "Movie"
        self.view.backgroundColor = grayColor
        moviePosterImageView.applyShadows()
    }
    
    private func updateUIInfo() {
        movieTitleLabel.text = movie.title
        movieDescriptionLabel.text = movie.overview
        movieDateReleaseLabel.text = movie.release_date
        movieGenresListLabel.text = movie.genres
        KFService.shared.setPosterImage(imageView: &moviePosterImageView, movie: movie)
    }
    
    func configureButton() {
        // If movie is favorited
        if movie.isFavorite {
            favoriteButton.setTitle("Remove from favorites", for: .normal)
        }
        // If not
        else if !movie.isFavorite {
            favoriteButton.setTitle("Add to Favorites", for: .normal)
        }
    }
    
    // MARK: - IBActions
    // Handle add to favorites button
    @IBAction func addFavoritesDidPressed(_ sender: Any) {
        // Check status
        // If is favorite
        if movie.isFavorite {
            favoriteButton.setTitle("Add to Favorites", for: .normal)
            RealmService.shared.updateFavoriteStatus(movie: movie, to: false)
        }
        // If is not favorite
        else if !movie.isFavorite {
            favoriteButton.setTitle("Remove from Favorites", for: .normal)
            RealmService.shared.updateFavoriteStatus(movie: movie, to: true)
        }
    }

}
