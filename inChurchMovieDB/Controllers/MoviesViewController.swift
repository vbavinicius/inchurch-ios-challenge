//
//  MoviesViewController.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 08/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class MoviesViewController: UIViewController {

    //MARK: - Variables
    // Variable that provides data for collection view
    var movies = RealmService.shared.read(object: MovieRealm.self).sorted(byKeyPath: "popularity", ascending: false)
    // Request related variables
    var currentPage = 1
    var isFetchingMovies = false
    // Cell Reuse ID
    let reuseIdentifier = "MoviesCollectionViewCell"
    // Refresh Control
    let refreshControl = UIRefreshControl()
    // Connection check
    let reachability = Reachability()!
    var noConnectionAlert: UIView!
    var noConnectionView: WarningView!
    
    //MARK: - IBOutlets
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure back button
        Helper.configureNavBarBackButton(vc: self)
        // Always check if is there new genres in API
        NetworkingService.shared.getGenresAndSaveToRealm()
        // Customize VC
        customizeVC()
        // Register NIB Cell
        registerCollectionViewCell()
        // Delegates
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        // Make a request to populate collection view
        NetworkingService.shared.getTrendingMovies(page: currentPage) {
            self.currentPage += 1
            self.moviesCollectionView.reloadData()
        }
        // Refresh Control
        setupRefreshControl()
        // Internet connection manager
        setupInternetConnectionManager()
    }

    override func viewDidAppear(_ animated: Bool) {
        // Reload data on view apper to reflect any changes
        moviesCollectionView.reloadData()
    }
    
    deinit {
        // Remove observer on deinit
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: self)
    }

    // MARK: - Setup methods
    private func customizeVC() {
        self.title = "Trending Movies"
        self.moviesCollectionView.backgroundColor = grayColor
        self.view.backgroundColor = grayColor
    }
    
    // MARK: - Refresh control
    private func setupRefreshControl() {
        refreshControl.tintColor = mainColor
        var attributes = [NSAttributedString.Key: AnyObject]()
        attributes[.foregroundColor] = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "Checking there is something new", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        moviesCollectionView.addSubview(refreshControl)
    }
    
    // Handle Refresh
    @objc func handleRefresh() {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (timer) in
            // Make a request to populate collection view
            NetworkingService.shared.getTrendingMovies(page: 1) {
                self.moviesCollectionView.reloadData()
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: - Infinite scroll methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Variables
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // Detect when user is close to bottom of screen
        if offsetY > contentHeight - 1500 && offsetY > scrollView.frame.height {
            // Check if is a fetch request in progress
            if !isFetchingMovies {
                // Fetch more movies from API
                fetchMoreMovies()
            }
        }
    }
    
    // Fetch more movies function
    func fetchMoreMovies() {
        // Set fetch request as in progress
        isFetchingMovies = true
        DispatchQueue.main.async {
            // Get more movies from API
            NetworkingService.shared.getTrendingMovies(page: self.currentPage) {
                self.currentPage += 1
                // Set fetch request as is not in progress
                self.isFetchingMovies = false
                // Reload data to include new received movies
                self.moviesCollectionView.reloadData()
            }
        }
    }
    
    // MARK: - Reachability manager
    func setupInternetConnectionManager() {
        // Setup warning views
        setupNoConnectionView()
        // Add observer to monitor connection changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged , object: reachability)
        // Start notifier
        do{
            try reachability.startNotifier()
        } catch {
            print("Could not strat notifier")
        }
    }
    
    func setupNoConnectionView() {
        // No connection little warning
        guard let tabBarY = tabBarController?.tabBar.frame.minY else {return}
        noConnectionAlert = UIView(frame: CGRect(x: 0, y: tabBarY - 20, width: self.view.frame.width, height: 20))
        noConnectionAlert.backgroundColor = .red
        noConnectionAlert.isHidden = true
        let warningLabel = UILabel(frame: CGRect(x: 0, y: 0, width: noConnectionAlert.frame.width, height: 20))
        warningLabel.text = "No internet connection"
        warningLabel.textAlignment = .center
        warningLabel.textColor = .white
        noConnectionAlert.addSubview(warningLabel)
        self.view.addSubview(noConnectionAlert)
        // No connection full view
        noConnectionView = WarningView.initFromNib()
        noConnectionView.imageView.image = UIImage(named: "error")
        noConnectionView.label.text = "An error has ocurred. Try to check your internet connection."
        noConnectionView.center = self.view.center
        noConnectionView.isHidden = true
        self.view.addSubview(noConnectionView)
    }
    
    // Handle connection status changed
    @objc func internetChanged(note:Notification)  {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            DispatchQueue.main.async {
                print("Internet is up!")
                self.setupConnectionStatus()
            }
        } else{
            DispatchQueue.main.async {
                print("Internet is down!")
                self.setupNoConnectionStatus()
            }
        }
    }
    
    // No Connection Action
    func setupNoConnectionStatus() {
        // If there is movies on screen
        if movies.count > 0 {
            // Display little warning
            self.noConnectionAlert.isHidden = false
            // If there is no movies on screen
        } else {
            // Display custom view
            self.moviesCollectionView.isHidden = true
            self.noConnectionView.isHidden = false
        }
    }
    
    // Connection actions
    func setupConnectionStatus() {
        // If there is movies on screen
        self.noConnectionAlert.isHidden = true
        self.noConnectionView.isHidden = true
        self.moviesCollectionView.isHidden = false
        self.fetchMoreMovies()
    }
}

// MARK: - Extension with collectionview methods and delegate methods
extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // Cell for row
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MoviesCollectionViewCell
        // Config cell
        cell.configureCell(movie: movies[indexPath.row])
        // Set cell as delegate of FavoriteSelectionDelegate
        cell.delegate = self
        return cell
    }
    
    // Register NIB cell function
    func registerCollectionViewCell() {
        let movieCollectionViewCellNib = UINib(nibName: "MoviesCollectionViewCell", bundle: nil)
        moviesCollectionView?.register(movieCollectionViewCellNib, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    // DataSource methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    // Go To DetailVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Deselect cell
        collectionView.deselectItem(at: indexPath, animated: true)
        // Go function
        Helper.goToDetailView(movie: movies[indexPath.row], currentVC: self)
    }
}

// MARK: - Extension with collectionview layout methods
extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    // Set cells sizes
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (collectionView.frame.width - 3 * 30) / 2
        return CGSize(width: cellWidth, height: (cellWidth * 1.5) + 40 )
    }
    // Set cells insets
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 30, bottom: 20, right: 30)
    }
    // Set cells spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

// MARK: - Extension to handle FavoriteSelectionDelegate
extension MoviesViewController: FavoriteSelectionDelegate {
    // Handle favorite button pressed on cell
    func favoriteButtonDidPressed(movie: MovieRealm, cell: UICollectionViewCell) {
        // If move is not favorite, mark as favorite, and vice-versa
        RealmService.shared.updateFavoriteStatus(movie: movie, to: !movie.isFavorite)
        // Reload changed cell
        if let cellIndexPath = moviesCollectionView.indexPath(for: cell) {
            moviesCollectionView.reloadItems(at: [cellIndexPath])
        }
    }

}
