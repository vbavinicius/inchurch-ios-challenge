//
//  FavoritesViewController.swift
//  inChurchMovieDB
//
//  Created by Vinícius Barcelos on 07/01/19.
//  Copyright © 2019 Vinícius Barcelos. All rights reserved.
//

import UIKit
import RealmSwift

class FavoritesViewController: UIViewController {

    //MARK: - Variables
    // Variable that provides data for table view
    let favoriteMovies = RealmService.shared.read(object: MovieRealm.self).filter("isFavorite = true").sorted(byKeyPath: "popularity", ascending: false)
    var favoriteMoviesFiltered = RealmService.shared.read(object: MovieRealm.self).filter("isFavorite = true").sorted(byKeyPath: "popularity", ascending: false)
    // Cell Reuse ID
    let reuseIdentifier = "FavoriteMovieUITableViewCell"
    // No favorites movies view
    var noFavoritesView: WarningView!
    var noResultsView: WarningView!
    // Search bar state
    var searchBarIsHidden: Bool!
    var isSearching: Bool = false
    
    //MARK: - IBOutlets
    @IBOutlet weak var favoritesTableView: UITableView!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure back button
        Helper.configureNavBarBackButton(vc: self)
        // Customize VC
        customizeVC()
        // Register NIB Cell
        registerFavoriteViewCell()
        // Delegates
        favoritesTableView.delegate = self
        favoritesTableView.dataSource = self
        searchBar.delegate = self
        // Setup no favorite movies view
        setupNoFavoritesView()
        setupNoResultsView()
        // Setup search bar
        setupSearchBar()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        favoritesTableView.reloadData()
        checkFavoritesCount()
        checkResultsCount()
    }

    // MARK: - IBActions
    @IBAction func searchButtonDidPressed(_ sender: Any) {
        searchBarIsHidden ? showSearchBar() : hideSearchBar()
    }
    
    // MARK: - Setup methods
    private func customizeVC() {
        self.title = "Favorite Movies"
        self.view.backgroundColor = grayColor
        self.favoritesTableView.backgroundColor = grayColor
        self.favoritesTableView.tableFooterView = UIView()
    }
    
    // Search bar config
    private func setupSearchBar() {
        // Inicitial state
        searchBarIsHidden = true
        topConstraint.constant = 0
        searchBar.alpha = 0
        isSearching = false
        // Config
        searchBar.showsCancelButton = true
        searchBar.barTintColor = grayColor
        searchBar.isTranslucent = true
        searchBar.layer.borderWidth = 0
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.backgroundImage = UIImage()
        if let buttonItem = searchBar.subviews.first?.subviews.last as? UIButton {
            buttonItem.setTitleColor(mainColor, for: .normal)
        }
    }
    
    // Show search bar
    private func showSearchBar() {
        // Check if searchbar is hidden
        guard searchBarIsHidden else {return}
        // Update table view top constraint
        topConstraint.constant = 56
        // Animate
        UIView.animate(withDuration: 0.5, animations: {
            // Set alpha to 1
            self.searchBar.alpha = 1
            // Update constraints
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            // Update search bar position status
            self.searchBarIsHidden = !self.searchBarIsHidden
        })
    }
    
    // Hide search bar
    private func hideSearchBar() {
        // Check if searchbar is hidden
        guard !searchBarIsHidden else {return}
        // Update table view top constraint
        topConstraint.constant = 0
        searchBar.text = ""
        view.endEditing(true)
        // Animate
        UIView.animate(withDuration: 0.5, animations: {
            // Set alpha to 0
            self.searchBar.alpha = 0
            // Update constraints
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            // Update search bar position status
            self.searchBarIsHidden = !self.searchBarIsHidden
        })
    }
    
 
    
    // Setup no favorite movies view
    func setupNoFavoritesView() {
        noFavoritesView = WarningView.initFromNib()
        noFavoritesView.imageView.image = UIImage(named: "green-ticket")
        noFavoritesView.label.text = "You do not have any favorite movies yet"
        noFavoritesView.center = self.view.center
        self.view.addSubview(noFavoritesView)
    }
    
    // Function to check if there is favorite movies
    func checkFavoritesCount() {
        if favoriteMovies.count == 0 {
            favoritesTableView.isHidden = true
            noFavoritesView.isHidden = false
        } else {
            favoritesTableView.isHidden = false
            noFavoritesView.isHidden = true
        }
    }
    
    // Setup no favorite movies view
    func setupNoResultsView() {
        noResultsView = WarningView.initFromNib()
        noResultsView.imageView.image = UIImage(named: "no-movies")
        noResultsView.label.text = "Movies with this name were not found"
        noResultsView.center = self.view.center
        self.view.addSubview(noResultsView)
    }
    
    // Function to check if there is favorite movies
    func checkResultsCount() {
        if favoriteMoviesFiltered.count == 0 && isSearching == true {
            favoritesTableView.isHidden = true
            noResultsView.isHidden = false
        } else if favoriteMoviesFiltered.count > 0 && isSearching == true {
            favoritesTableView.isHidden = false
            noResultsView.isHidden = true
        } else if isSearching == false {
            favoritesTableView.isHidden = false
            noResultsView.isHidden = true
        }
    }
}

// MARK: - Extension with table methods and delegate methods
extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    // Register NIB cell function
    func registerFavoriteViewCell() {
        let movieFavoriteTableViewCellNib = UINib(nibName: "FavoriteMovieUITableViewCell", bundle: nil)
        favoritesTableView.register(movieFavoriteTableViewCellNib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    // Cell for row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FavoriteMovieUITableViewCell
        // Config cell
        if isSearching {
            cell.configureCell(movie: favoriteMoviesFiltered[indexPath.row])
        } else {
            cell.configureCell(movie: favoriteMovies[indexPath.row])
        }
        return cell
    }
    
    // DataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return favoriteMoviesFiltered.count
        } else {
            return favoriteMovies.count
        }
    }

    // Cell Height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    // Go To DetailVC
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("here")
        // Deselect cell
        tableView.deselectRow(at: indexPath, animated: true)
        // Go function
        if isSearching {
            Helper.goToDetailView(movie: favoriteMoviesFiltered[indexPath.row], currentVC: self)
        } else {
            Helper.goToDetailView(movie: favoriteMovies[indexPath.row], currentVC: self)
        }
 
    }

    
    // Swipe table view cell functions
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        // Delete action
        if editingStyle == .delete {
            // Remove from Realm
            var moveToRemove: MovieRealm!
            if isSearching {
                moveToRemove = favoriteMoviesFiltered[indexPath.row]
            } else {
                moveToRemove = favoriteMovies[indexPath.row]
            }
            RealmService.shared.updateFavoriteStatus(movie: moveToRemove, to: false)

        }
        // Delete row with movie
        tableView.deleteRows(at: [indexPath], with: .fade)
        // Reload table view data
        tableView.reloadData()
        // Check if there is any favorite in order to present empty state screen
        checkFavoritesCount()
    }
}

// MARK: - Extension with searchbar methods and delegate methods
extension FavoritesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Check is text is empty
        if searchText.isEmpty{
            isSearching = false
        } else {
            isSearching = true
        }
        // Build a predicate to filter array with searchText
        let predicate = NSPredicate(format: "title CONTAINS[c] %@", searchText.lowercased())
        favoriteMoviesFiltered = favoriteMovies.filter(predicate)
        // Check if there any result
        checkResultsCount()
        // Reload table view
        favoritesTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        guard !searchBarIsHidden else {return}
        // Set isSearching as false
        isSearching = false
        // Hide search bar
        hideSearchBar()
        // Check if there any result
        checkResultsCount()
        // Reload table view
        favoritesTableView.reloadData()
    }
}


